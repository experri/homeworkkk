package com.main.homework;

import java.util.Random;
import java.util.Scanner;

public class casino {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Press any key to start the game...");

        int attemptCount = 0;
        int sameDigitsCount = 0;
        final int MAX_ATTEMPTS = 100;
        final double SAME_DIGITS_PROBABILITY = 5;

        while (scanner.hasNext()) {
            if (attemptCount >= MAX_ATTEMPTS) {
                System.out.println("Game stopped.");
                break;
            }

            String randomDigits;
            do {
                randomDigits = generateRandomDigits();
                System.out.println("Random number: " + randomDigits);
                attemptCount++;
                if (checkForEqualDigits(randomDigits)) {
                    sameDigitsCount++;
                }
            } while (!checkForEqualDigits(randomDigits) && attemptCount < MAX_ATTEMPTS);
        }

    }

    private static String generateRandomDigits() {
        Random random = new Random();
        int digit1 = random.nextInt(10);
        int digit2 = random.nextInt(10);
        int digit3 = random.nextInt(10);

        return String.format("%d%d%d", digit1, digit2, digit3);
    }

    private static boolean checkForEqualDigits(String digits) {
        return digits.charAt(0) == digits.charAt(1) && digits.charAt(1) == digits.charAt(2);
    }

    public void main() {
    }
}



