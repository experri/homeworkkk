package com.company.homework;

import java.util.Random;
import java.util.Scanner;

public class homework2 {
    public static void main(String[] args) {
        char[][] targetBoard = new char[5][5];
        boolean[][] shotBoard = new boolean[5][5];
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);

        initializeTarget(targetBoard, random);
        System.out.println("All Set. Get ready to rumble!");

        while (true) {
            printBoard(targetBoard, shotBoard);

            int row, col;
            do {
                System.out.print("Enter row (1-5): ");
                row = scanner.nextInt() - 1;
                System.out.print("Enter column (1-5): ");
                col = scanner.nextInt() - 1;
            } while (!isValidInput(row, col) || shotBoard[row][col]);

            shotBoard[row][col] = true;

            if (targetBoard[row][col] == 'x') {
                printBoard(targetBoard, shotBoard);
                System.out.println("You have won!");
                break;
            }
        }
    }

    public static void initializeTarget(char[][] targetBoard, Random random) {
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                targetBoard[i][j] = '-';
            }
        }
        int targetRow = random.nextInt(5);
        int targetCol = random.nextInt(5);
        targetBoard[targetRow][targetCol] = 'x';
    }

    public static void printBoard(char[][] targetBoard, boolean[][] shotBoard) {
        System.out.println("0 | 1 | 2 | 3 | 4 | 5 |");
        for (int i = 0; i < 5; i++) {
            System.out.print((i + 1) + " ");
            for (int j = 0; j < 5; j++) {
                if (shotBoard[i][j]) {
                    System.out.print("| " + (targetBoard[i][j] == '-' ? "*" : targetBoard[i][j]) + " ");
                } else {
                    System.out.print("| - ");
                }
            }
            System.out.println("|");
        }
    }

    public static boolean isValidInput(int row, int col) {
        return row >= 0 && row < 5 && col >= 0 && col < 5;
    }
}



