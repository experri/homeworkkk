package org.homework;


import java.util.Random;
import java.util.Scanner;

public class homework1 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Let the game begin!");
        System.out.print("Enter a name: ");
        String line = scanner.next();


        Random rand = new Random();
        StringBuilder attempts = new StringBuilder();
        int[] userAttempts = new int[1000];
        int attemptCount = 0;


        while (true) {
            int c = (int) rand.nextInt(10) + 1;

            System.out.print("Enter your number: ");
            while (!scanner.hasNextInt()) {
                System.out.println("You entered a non-integer value.");
                scanner.next();
                System.out.print("Enter your number: ");
            }

            int g = scanner.nextInt();

            attempts.append(g).append(", ");
            userAttempts [attemptCount++] = g;

            System.out.println("The system has chosen a number: " + c);

            if (g == c) {
                System.out.printf("Congratulations, %s! Your numbers: %s%s%n", line, attempts, g); break;
            } else if (g < c) {
                System.out.printf("%s, your number is too small. Please, try again.", line);
            } else {
                System.out.printf("%s, your number is too big. Please, try again.", line);
            }
        }
    }
}


