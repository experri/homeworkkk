package com.main.homework;

import java.util.Random;
import java.util.Scanner;

public class casino2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Press any key to start the game...");

        while (scanner.hasNext()) {
            System.out.println("New attempt:");
            String randomDigits = generateRandomDigits();
            System.out.println("Random number: " + randomDigits);

            if (checkForEqualDigits(randomDigits)) {
                System.out.println("Three same numbers " + randomDigits + " detected.");
                System.out.println("Press any key to play again, or type 'exit' to quit...");
                String input = scanner.next();
                if (input.equalsIgnoreCase("exit")) {
                } break;
            } else {
                System.out.println("Press any key to try again...");
                scanner.next();
            }
        }

        scanner.close();
    }

    private static String generateRandomDigits() {
        Random random = new Random();
        int digit1 = random.nextInt(10);
        int digit2 = random.nextInt(10);
        int digit3 = random.nextInt(10);

        return String.format("%d%d%d", digit1, digit2, digit3);
    }
    private static boolean checkForEqualDigits(String digits) {
        return digits.charAt(0) == digits.charAt(1) && digits.charAt(1) == digits.charAt(2);
    }

}
