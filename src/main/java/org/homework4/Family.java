package org.homework4;

import java.io.Serializable;
import java.util.*;

@SuppressWarnings("ALL")
public class Family implements Serializable {
    private Human mother;
    private Human father;
    private List<Human> children;
    private Pet pet;
    private Set<Pet> pets;
    private String prettyFormat;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);
        this.children = new ArrayList<>();
        this.pets = new HashSet<>();
    }
    public Family(Human mother, Human father, Set<Pet> pets) {
        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);
        this.children = new ArrayList<>();
        this.pets = new HashSet<>();
    }
    public Family(Human mother, Human father, Pet pet) {
        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);
        this.children = new ArrayList<>();
        this.pet = pet;
    }


    //child
    public void addChild(Human child) {
        children.add(child);
    }

    public void deleteChild(Human child) {
        children.remove(child);
    }

    public int countFamily() {
        return 2 + children.size();
    }

    public List<Human> getChildren() {
        return children;
    }
    public void setChildren(List<Human> children) {
        this.children = children;
    }
    //mother
    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    // father
    public Human getFather() {
        return father;
    }
    public void setFather(Human father) {
        this.father = father;
    }


    // pet1
    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    // pet2
    public Set<Pet> getPets (Set<Pet> pets) {
        return pets;
    }
    public void setPet(Set<Pet> pets) {
        this.pets = (Set<Pet>) pets;
    }
    public void deleteAllChildrenOlderThen(int age) {
    }

    public Human bornChild(String boyName, String girlName) {
        return null; }

    public Set<Pet> getPets() {
        return (Set<Pet>) this.pets;
    }

    public void addPet(Pet pet) {
        pets.add(pet);
    }

    public void prettyFormat(String prettyFormat) {
        this.prettyFormat = prettyFormat;
    }


    // feed
    public void feedPet() {
        System.out.println("Я годую свого улюбленця");
    }

    // greetPet
    private String greetPet;
    public String getGreetPet () {
        return this.greetPet;
    }
    public void setGreetPet (String greetPet) {
        this.greetPet = greetPet;
    }
    public void greetPet() {
        System.out.println("Привіт, " + pet.getNickname());
    }

    // describePet
    private String describePet;
    public String getDescribePet() {
        return this.describePet;
    }
    public void setDescribePet (String describePet) {
        this.describePet = describePet;
    }
    public void describePet() {
        String trickLevelDescription = (pet.getTrickLevel() > 50) ? "дуже хитрий" : "майже не хитрий";
        System.out.println("У мене є " + pet.getSpecies() + ", йому " + pet.getAge() + " років, він " + trickLevelDescription);
    }


    @Override
    public String toString() {
        return "family:  \n        "
                + "\nmother:" + mother
                + "\nfather:" + father
                + "\nchildren: " + Arrays.toString(new List[]{children})
                + "\npet: " + pets + '}';
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Family with mother " + mother.getName() + " and father " + father.getName() + " is being finalized.");
        super.finalize();
    }

}
