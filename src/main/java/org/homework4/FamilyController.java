package org.homework4;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public class FamilyController implements Serializable {

    private final FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    // Отримати список всіх сімей
    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    // Вивести на екран всі сім'ї
    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    // Знайти сім'ї з кількістю людей більше за вказану
    public void getFamiliesBiggerThan(int number) {
        familyService.getFamiliesBiggerThan(number);
    }

    // Знайти сім'ї з кількістю людей менше за вказану
    public void getFamiliesLessThan(int number) {
        familyService.getFamiliesLessThan(number);
    }

    // Підрахувати кількість сімей з певною кількістю людей
    public int countFamiliesWithMemberNumber(int number) {
        return familyService.countFamiliesWithMemberNumber(number);
    }

    // Створити нову сім'ю
    public void createNewFamily(Human mother, Human father) {
        familyService.createNewFamily(mother, father);
    }

    // Народити нову дитину
    public Family bornChild(Family family, String boyName, String girlName) {
        return familyService.bornChild(family, boyName, girlName);
    }

    // Усиновити дитину
    public Family adoptChild(Family family, Human child) {
        return familyService.adoptChild(family, child);
    }

    // Видалити всіх дітей старшими за вказаний вік
    public void deleteAllChildrenOlderThen(int age) {
        familyService.deleteAllChildrenOlderThen(age);
    }

    // Підрахувати кількість сімей
    public int count() {
        return familyService.count();
    }

    // Отримати сім'ю за індексом
    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    // Отримати список тварин у сім'ї за індексом
    public Set<Pet> getPets(int familyIndex) {
        return familyService.getPets(familyIndex);
    }
    // Додати тварину до сім'ї
    public Family addPet(int familyIndex, Pet pet) {
        return familyService.addPet(familyIndex, pet);
    }

    // Видалити сім'ю за індексом у списку - видаляє сім'ю із БД
    public boolean deleteFamily(Family family, int index) {
        return familyService.deleteFamily(family, index);
    }

    //Видалити сім'ю за індексом у списку - видаляє сім'ю із БД
    public void deleteFamilyByIndex(int index) {
        familyService.deleteFamilyByIndex(index);
    }



}
