package org.homework4;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService implements Serializable {

    private final FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    // одержати список всіх сімей
    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    // вивести на екран усі сім'ї (в індексованому списку) з усіма членами сім'ї
    public void displayAllFamilies() {
        familyDao.getAllFamilies().forEach(System.out::println);

//        List<Family> families = familyDao.getAllFamilies();
//        if (families != null) {
//            for (int i = 0; i < families.size(); i++) {
//                System.out.println("Family " + (i + 1) + ": " + families.get(i));
//            }
//        } else {
//            System.out.println("There are no families in the database.");
//        }
    }

    // Видалити сім'ю за індексом у списку - видаляє сім'ю із БД
    public boolean deleteFamily(Family family, int index) {
        family = familyDao.getFamilyByIndex(index);
        if (family != null) {
            familyDao.deleteFamily(family);
            return true;
        }
        return false;
    }

    // знайти сім'ї з кількістю людей більше ніж
//    public List<Family> getFamiliesBiggerThan(int number) {
//        List<Family> families = familyDao.getAllFamilies();
//            for (Family family : families) {
//                if (family.countFamily() > number) {
//                    System.out.println(family);
//                }
//            }
//        return families;
//    }

    public void getFamiliesBiggerThan(int count) {
        familyDao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() > count)
                .forEach(System.out::println);
    }

    // знайти сім'ї з кількістю людей менше ніж
//    public void getFamiliesLessThan(int number) {
//        List<Family> families = familyDao.getAllFamilies();
//            for (Family family : families) {
//                if (family.countFamily() < number) {
//                    System.out.println(family);
//                }
//            }
//    }
    public void getFamiliesLessThan(int count) {
        familyDao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() < count)
                .forEach(System.out::println);
    }

    // підрахувати число сімей з кількістю людей, що дорівнює переданому числу
    public int countFamiliesWithMemberNumber(int number) {
//        List<Family> families = familyDao.getAllFamilies();
//        int count = 0;
//        for (Family family : families) {
//            if (family.countFamily() == number) {
//                count++;
//                System.out.println("Число сімей з кількістю людей, що дорівнює переданому числу:" + count);
//            } else {
//                System.out.println("Число сімей з кількістю людей, що дорівнює переданому числу: 0");
//
//            }
//
//        }
//        return count;
        return (int) familyDao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() == number)
                .count();
    }

    // Створити нову сім'ю з двома заданими людьми і зберегти її в базі даних
    public void createNewFamily(Human mother, Human father) {
        familyDao.saveFamily(new Family(mother, father));
    }

    // Видалити сім'ю за індексом у списку - видаляє сім'ю із БД
    public void deleteFamilyByIndex(int index) {
        familyDao.deleteFamilyByIndex(index);
    }

    // Народити нову дитину в сім'ї за індексом у списку, з урахуванням імені дитини та збереженням в базі даних
    public Family bornChild(Family family, String boyName, String girlName) {
        if (family != null) {
            String childName;
            Human.Gender gender;
            if (Math.random() < 0.5) {
                childName = boyName;
                gender = Human.Gender.MALE;
            } else {
                childName = girlName;
                gender = Human.Gender.FAMALE;
            }
            Human child = new Human(childName, family.getFather().getSurname(), family.getFather().getBirthDate(), gender);
            family.addChild(child);
            familyDao.saveFamily(family);
            return family;
        } else {
            return null;
        }
    }

    // Усиновити дитину в сім'ї за індексом у списку, збереженням змін в базі даних
    public Family adoptChild(Family family, Human child) {
        if (family != null) {
            family.addChild(child);
            familyDao.saveFamily(family);
            return family;
        }
        return null;
    }

    // Видалити дітей старше переданого віку з усіх сімей і зберегти зміни в базі даних
    public void deleteAllChildrenOlderThen(int age) {
        List<Family> families = familyDao.getAllFamilies();
        List<Family> allFamilies = new ArrayList<>(families);

        for (Family family : allFamilies) {
            List<Human> children = new ArrayList<>(family.getChildren());

            for (Human child : children) {
                LocalDate birthDate = child.getBirthDate().toInstant()
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate();
                int childAge = Period.between(birthDate, LocalDate.now()).getYears();

                if (childAge > age) {
                    family.deleteChild(child);
                }
            }
        }
    }

    // Повернути кількість сімей у базі даних
    public int count() {
        return familyDao.getAllFamilies().size();
    }

    // Повернути Family за індексом у списку
    public Family getFamilyById(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    // Повернути список свійських тварин у сім'ї за індексом у списку
    public Set<Pet> getPets(int familyIndex) {
        Family family = familyDao.getFamilyByIndex(familyIndex);
        if (family != null) {
            return family.getPets();
        }
        return null;
    }

    // Додати нового вихованця в сім'ю за індексом у списку та зберегти зміни в базі даних
    public Family addPet(int familyIndex, Pet pet) {
        Family family = familyDao.getFamilyByIndex(familyIndex);
        if (family != null) {
            family.addPet(pet);
            familyDao.saveFamily(family);
            return family;
        }
        return null;
    }

}
