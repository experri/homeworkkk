package org.homework4;

import org.homework4.Console.Console;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {

////        // Створення сімей
//        Human mother = new Human("Joana", "May", 1995, 123);
//        Human father = new Human("Karl", "May", 1995, 155);
//        Family family1 = new Family(mother, father);
//
//        Human child1 = new Human("Alice", "May", 2024, 76, mother, father);
//        family1.addChild(child1);
//        Pet pet1 = new DomesticCat("Dora", 7);
//        family1.setPet(pet1);
//
//        // Виведення інформації про кожну людину
////        System.out.println(mother);
////        System.out.println(father);
////        System.out.println(child1);
////        System.out.println(pet1);
//        System.out.println(family1);
//
//        // Створення повноцінної сім'ї
//        Human mother2 = new Human("Mary", "Winston", 1989, 90);
//        Human father2 = new Human("Piter", "Winston", 1987, 150);
//        Family family2 = new Family(mother2, father2);
//
//        Human child2 = new Human("Emmy", "Winston", 2019, 132, mother2, father2);
//        family2.addChild(child2);
//
//        Pet pet2 = new Dog("Mari", 3);
//        family2.setPet(pet2);
//
//        // Виклик методів у дитини та її вихованця
//        family2.greetPet();
//        family2.describePet();
//        pet1.respond();
//        pet1.eat();
//        pet1.foul();
//
//        // homework 5
//        Human mother3 = new Human("Luna", "Li", 1977, 143);
//        Human father3 = new Human("Yan", "Li", 1975, 134);
//        Family family3 = new Family(mother3, father3);
//        Pet pet3 = new RoboCat("Pretty", 1);
//        pet3.setHabits("Running");
//        family3.setPet(pet3);
//        father3.addSchedule(Human.DayOfWeek.SATURDAY, "Read the book");
//        mother3.addSchedule(Human.DayOfWeek.THURSDAY, "Go to market");
//
//        System.out.println("\n");
//        System.out.println(mother3);
//        System.out.println(father3);
//        System.out.println(family3);
//        System.out.println(pet3);
//        family3.greetPet();
//        family3.describePet();
//
//
//        // woman
//        Woman Valeria = new Woman("Valeria", "Tkach", 2000);
//        Pet petV = new Dog("Val", 10);
//        petV.setHabits("Eating");
//        Valeria.addSchedule(Human.DayOfWeek.MONDAY, "Go to work");
//
//
//        System.out.println(Valeria);
//        System.out.println(petV);
//
//
//        System.out.println("\n");


        // FamilyController

        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

//
//        familyController.createNewFamily(
//                new Human("Celina", "Kilian", 12 / 3 / 2001, 120),
//                new Human("John", "Kilian", 12 / 5 / 2000, 121));
//        familyController.addPet(0, new Dog("Jack", 3));
//        familyController.getPets(0);
//        familyController.bornChild(familyService.getFamilyById(0), "Li", "Vi");
//        familyController.adoptChild(familyService.getFamilyById(0), new Human("Julia", "Kilian", 7 / 12 / 2022, 126));
//        familyController.createNewFamily(
//                new Human("Katty", "Portman", 23 / 10 / 2024, 140),
//                new Human("Will", "Portman", 1 / 2 / 2002, 130));
//        familyController.count();
//        familyController.displayAllFamilies();
//        familyController.deleteAllChildrenOlderThen(20);
//        familyController.deleteFamilyByIndex(1);
//        familyController.getAllFamilies();
//        familyController.displayAllFamilies();
//        familyController.countFamiliesWithMemberNumber(2);


        Console consoleMenu = new Console();
        consoleMenu.Start();
    }

    }



