package org.homework4.Console;

import org.homework4.*;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;


public class Console implements Serializable {

    private FamilyDao familyDao;
    private final FamilyService familyService;
    private final FamilyController familyController;
    private final Scanner scanner;


    public Console() {
        this.familyDao = (FamilyDao) loadObjectFromFile("family.ser");

        if(this.familyDao == null){
            this.familyDao = new CollectionFamilyDao();
        }

        this.familyService = new FamilyService(familyDao);
        this.familyController = new FamilyController(familyService);
        this.scanner = new Scanner(System.in);

    }

    public void Start() throws IOException {

        System.out.printf("Hello! Please, choice a command.\n");

        showMenu();
        while (true) {

            try {
                int choice = scanner.nextInt();
                switch (choice) {

                    case 1 -> createTestFamily();
                    case 2 -> showAllFamily();
                    case 3 -> showFamiliesBiggerThan();
                    case 4 -> showFamiliesLessThan();
                    case 5 -> showFamilies();
                    case 6 -> createFamily();
                    case 7 -> deleteFamily();
                    case 8 -> correctFamily();
                    case 9 -> deleteChildBiggerThan();
                    case 10 -> exit();

                    default -> throw new FamilyException("Invalid menu option.");
                }
            } catch (FamilyException | NumberFormatException | ParseException e) {
                System.out.println(e.getMessage());
            }
        }
    }
        private void showMenu () {
            System.out.println("1. Заповнити тестовими даними.");
            System.out.println("2. Відобразити весь список сімей.");
            System.out.println("3. Відобразити список сімей, де кількість людей більша за задану.");
            System.out.println("4. Відобразити список сімей, де кількість людей менша за задану.");
            System.out.println("5. Підрахувати кількість сімей, де кількість членів дорівнює.");
            System.out.println("6. Створити нову родину.");
            System.out.println("7. Видалити сім'ю за індексом сім'ї у загальному списку.");
            System.out.println("8. Редагувати сім'ю за індексом сім'ї у загальному списку.");
            System.out.println("9. Видалити всіх дітей старше віку.");
            System.out.println("10. Exit.");

        }

        private void createTestFamily () throws ParseException {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

            familyController.createNewFamily(new Human("Celina", "Kilian", simpleDateFormat.parse("23/05/2000"), 120),
                    new Human("John", "Kilian", simpleDateFormat.parse("12/05/2000"), 120));
            familyController.addPet(0, new Dog("Jack", 3));
            familyController.getPets(0);
            familyController.bornChild(familyService.getFamilyById(0), "Li", "Vi");
            familyController.createNewFamily(new Human("Katty", "Portman", simpleDateFormat.parse("23/10/2024"), 123),
                    new Human("Will", "Portman", simpleDateFormat.parse("01/02/2002"), 100));
            System.out.println("Test family created\n");
            System.out.println("Press any button to exit to the menu");
            scanner.next();
            showMenu();
        }

        private void showAllFamily(){
        familyController.displayAllFamilies();
            System.out.println("Press any button to exit to the menu");
            scanner.next();
            showMenu();
        }

        private void showFamiliesBiggerThan(){
            System.out.print("Enter a number: ");
            int BiggerNumber = scanner.nextInt();
        familyController.getFamiliesBiggerThan(BiggerNumber);
        }

        private void showFamiliesLessThan(){
            System.out.print("Enter a number: ");
            int LessNumber = scanner.nextInt();
            familyController.getFamiliesLessThan(LessNumber);
        }

        private void showFamilies(){
            System.out.println("Enter a number: ");
            int memberNumber = scanner.nextInt();
            familyController.countFamiliesWithMemberNumber(memberNumber);
        }

        private void createFamily() throws ParseException {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            System.out.print("Enter a mother firstname: ");
            String name = scanner.next();
            System.out.print("Enter a mother lastname: ");
            String lastname = scanner.next();;
            System.out.print("Enter a mother birthday: ");
            String birthday = scanner.next();
            System.out.print("Enter a mother IQ: ");
            int iq = scanner.nextInt();
            System.out.print("Enter a father firstname: ");
            String name1 = scanner.next();
            System.out.print("Enter a father lastname: ");
            String lastname1 = scanner.next();
            System.out.print("Enter a father day of birthday: ");
            String birthday1 = scanner.next();
            System.out.print("Enter a father IQ: ");
            int iq1 = scanner.nextInt();

            familyController.createNewFamily(
                    new Human(name, lastname, simpleDateFormat.parse(birthday), iq),
                    new Human(name1, lastname1, simpleDateFormat.parse(birthday1), iq1));
            System.out.println("New family created.");
            System.out.println("Press any button to exit to the menu");
            scanner.next();
            showMenu();
        }

    private void deleteFamily(){
        System.out.println("Enter ID family: ");
        int idFamilyDel = scanner.nextInt();
        familyController.deleteFamilyByIndex(idFamilyDel);
        System.out.println("Family was deleted.");
    }

    private void correctFamily() throws IOException, ParseException {
        System.out.println("Choice a command: ");
            while (true) {
                showCorrect();
                int choice = scanner.nextInt();
                switch (choice) {
                    case 1 -> {
                        System.out.println("Enter ID family: ");
                        int idFamilyCorrect = scanner.nextInt();
                        System.out.println("Enter a boy name: ");
                        String boyName = scanner.next();
                        System.out.println("Enter a girl name: ");
                        String girlName = scanner.next();
                        familyController.bornChild(familyService.getFamilyById(idFamilyCorrect), boyName, girlName);
                        System.out.println("Child born.");
                    }

                    case 2 -> {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        System.out.println("Enter ID family: ");
                        int idFamilyCorrect2 = scanner.nextInt();
                        familyController.getFamilyById(idFamilyCorrect2);
                        System.out.println("Enter a child name: ");
                        String childName = scanner.next();
                        System.out.println("Enter a child surname: ");
                        String childSurname = scanner.next();
                        System.out.println("Enter a child birthday: ");
                        String childBirthday = scanner.next();
                        System.out.println("Enter a child IQ: ");
                        int childIq = scanner.nextInt();
                        familyController.adoptChild(familyService.getFamilyById(idFamilyCorrect2),
                                new Human(childName, childSurname, simpleDateFormat.parse(childBirthday), childIq));
                        System.out.println("Child adopted");
                    }
                    case 3 -> Start();

                    default -> { showMenu();
                        System.out.println("Sorry, I don't understand you, please try again.");
                    }
                }
            }
    }

    private void showCorrect(){
        System.out.println("1. Born child.");
        System.out.println("2. Adopt child.");
        System.out.println("3. Return to menu.");

    }

    private void deleteChildBiggerThan(){
        System.out.println("Indicate the year of birth of the child: ");
        int yearChild = scanner.nextInt();
        familyController.deleteAllChildrenOlderThen(yearChild);
        System.out.println("Child is deleted.");
    }

        private void exit () {
            saveObjectToFile("family.ser", familyDao);
            System.out.println("Exiting...");
            System.exit(0);
        }

    // save
    private void saveObjectToFile(String filename, Object obj) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename))) {
            oos.writeObject(obj);
        } catch (IOException e) {
            System.out.println("Помилка при зберіганні: " + e.getMessage());
        }
    }

    // load
    private Object loadObjectFromFile(String filename) {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename))) {
            return ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Помилка при загрузці: " + e.getMessage());
            return null;
        }
    }

}
