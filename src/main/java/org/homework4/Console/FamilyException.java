package org.homework4.Console;

public class FamilyException extends Exception {
    public FamilyException(String message) {
        super(message);
    }
}
