package org.homework4;

import java.io.Serializable;

public class DomesticCat extends Pet  implements Serializable {
    public DomesticCat(String nickname, int age) {
        super(nickname, age, Species.DOMESTICCAT);
    }

    @Override
    public void respond() {
        System.out.println("...");
    }

    @Override
    public void describe() {
        System.out.println("I am a DOMESTIC CAT");
    }

    @Override
    public void foul() {
        System.out.println("Потрібно добре замести сліди...");
    }
}
