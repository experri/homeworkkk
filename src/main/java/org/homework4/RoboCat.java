package org.homework4;

import java.io.Serializable;

public class RoboCat extends Pet  implements Serializable {
    public RoboCat(String nickname, int age) {
        super(nickname, age, Species.ROBOCAT);
    }

    @Override
    public void respond() {
        System.out.println("...");
    }

    @Override
    public void describe() {
        System.out.println("Fish does not foul.");
    }

    @Override
    public void foul() {
        System.out.println("RoboCat does not foul.");
    }
}
