package org.homework4;

import java.util.List;

public interface FamilyDao {

    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    boolean deleteFamilyByIndex(int index);
    boolean deleteFamily(Family family);
    void saveFamily(Family family);
    void createNewFamily(Human father, Human mother);
    Family bornChild(Family family, String boyName, String girlName);
    Family adoptChild(Family family, Human child);
    void deleteAllChildrenOlderThen(int age);
    int count();
    List<Pet> getPets(int familyIndex);
    Family addPet(int familyIndex, Pet pet);





}
