package org.homework4;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao, Serializable {

    private List<Family> families;

    public CollectionFamilyDao() {
        this.families = new ArrayList<>();
    }
    // повертає проіндексований список всіх сімей
    @Override
    public List<Family> getAllFamilies() {
        return this.families;
    }

    // приймає індекс сім'ї із загального списку - повертає сім'ю за вказаним індексом.
    @Override
    public Family getFamilyByIndex(int index) {
        if (index >= 0 && index < families.size()) {
        return families.get(index);
    }
        return null;
    }

    // видаляє сім'ю із заданим індексом, якщо такий індекс існує;
    @Override
    public boolean deleteFamilyByIndex(int index) {
        if (index >= 0 && index < families.size()) {
            families.remove(index);
        }
        return false;
    }

    // видаляє сім'ю якщо така існує у списку
    @Override
    public boolean deleteFamily(Family family) {
        families.remove(family);
        return false;
    }

    // оновлює наявну сім'ю в БД, якщо така вже існує, зберігає в кінець списку - якщо ні.
    @Override
    public void saveFamily(Family family) {
        families.add(family);

    }

    @Override
    public void createNewFamily(Human father, Human mother) {
        Family family = new Family(father, mother);
        families.add(family);
    }

    @Override
    public Family bornChild(Family family, String boyName, String girlName) {
        return null;
    }

    @Override
    public Family adoptChild(Family family, Human child) {
        return null;
    }

    @Override
    public void deleteAllChildrenOlderThen(int age) {
        List<Family> allFamilies = new ArrayList<>(families);
        for (Family family : allFamilies) {
            List<Human> children = new ArrayList<>(family.getChildren());

            for (Human child : children) {
                LocalDate birthDate = child.getBirthDate().toInstant()
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate();
                int childAge = Period.between(birthDate, LocalDate.now()).getYears();

                if (childAge > age) {
                    family.deleteChild(child);
                }
            }
        }
    }

    @Override
    public int count() {
        return 0;
    }

    @Override
    public List<Pet> getPets(int familyIndex) {
        return null;
    }

    @Override
    public Family addPet(int familyIndex, Pet pet) {
        return null;
    }

}