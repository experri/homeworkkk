package org.homework4;

import java.io.Serializable;

public class Fish extends Pet  implements Serializable {
    public Fish(String nickname, int age) {
        super(nickname, age, Species.FISH);

    }

    @Override
    public void respond() {
        System.out.println("...");
    }

    @Override
    public void describe() {
        System.out.println("I am a fish.");
    }

    @Override
    public void foul() {
        System.out.println("Fish does not foul.");
    }
}
