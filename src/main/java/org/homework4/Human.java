package org.homework4;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

@SuppressWarnings("ALL")
public class Human implements Serializable {

// human

    private String name;
    private String surname;
    private Date birthDate;
    private int iq;
    private Family family;
    private Map<DayOfWeek, List<String>> schedule;
    private Gender gender;

    enum DayOfWeek {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY
}

enum Gender {
    MALE,
    FAMALE
}

public Human(String name, String surname, Date birthDate, int iq) {
    this.name = name;
    this.surname = surname;
    this.birthDate = birthDate;
    this.iq = iq;
    this.schedule = new HashMap<>();
    this.gender = gender;
}

public Human(String name, String surname, Date birthDate, int iq, Human mother, Human father){
    this.name = name;
    this.surname = surname;
    this.birthDate = birthDate;
    this.iq = iq;
    this.schedule = new HashMap<>();
    this.gender = gender;
}

public Human(String name, String surname, Date birthDate,
             Gender gender, int iq, Pet pet,Map<DayOfWeek,
             List<String>> schedule) {

    this.name = name;
    this.surname = surname;
    this.birthDate = birthDate;
    this.iq = iq;
    this.schedule = new HashMap<>();
    this.gender = gender;
}

public Human(String name, String surname , Date birthDate, Gender gender) {
    this.name = name;
    this.birthDate = birthDate;
    this.gender = gender;
    this.schedule = new HashMap<>();
}

// schedule
public void addSchedule(DayOfWeek day, String task) {
    schedule.put(day, Collections.singletonList(task));
}

// name
public String getName() {
    return name;
}
public void setName(String name) {
    this.name = name;
}

// surname
public String getSurname() {
    return surname;
}
public void setSurname(String surname) {
    this.surname = surname;
}

// age
 public Date getBirthDate() {
    return birthDate;
}

public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
}
public String describeAge() {
    LocalDate birthDateLocal = birthDate.toInstant()
            .atZone(ZoneId.systemDefault())
            .toLocalDate();
    LocalDate currentDate = LocalDate.now();
    Period age = Period.between(birthDateLocal, currentDate);
    return String.format("Вік: %d років, %d місяців, %d днів",
            age.getYears(), age.getMonths(), age.getDays());
    }

private long convertToMillis(String birthDate) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    LocalDate date = LocalDate.parse(birthDate, formatter);
    return date.toEpochDay();
    }

// iq
public int getIq() {
    return iq;
}
public void setIq(int iq) {
    this.iq = iq;
}

// family
public Family getFamily() {
    return family;
}
public void setFamily(Family family) {
    this.family = family;
}

// ununiqueAction
 public void uniqueAction() {
}
public void greetPet (){
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Name: ").append(name).append("\n");
        sb.append("Schedule:\n");
        for (Map.Entry<DayOfWeek, List<String>> entry : schedule.entrySet()) {
            sb.append(entry.getKey().name()).append(": ").append(entry.getValue()).append("\n");
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        return  "{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate=" + formatter.format(birthDate) +
                ", iq=" + iq +
                ", gender=" + gender +
                "}" + "task=" + "\n"
                + sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate &&
                iq == human.iq &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Human " + name + " " + surname + " is being finalized.");
        super.finalize();
    }
}

