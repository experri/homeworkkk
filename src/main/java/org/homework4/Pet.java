package org.homework4;

import java.io.Serializable;
import java.util.*;

@SuppressWarnings("ALL")
public abstract class Pet  implements Serializable {

    protected Species species;
    protected String nickname;
    protected int age;
    protected int trickLevel;
    protected Set<String> habits;

 enum Species {
     DOG,
     CAT,
     FISH,
     ROBOCAT,
     DOMESTICCAT,
     UNKNOWN
 }

public Pet(String nickname) {
    this.species = species;
    this.nickname = nickname;
    this.habits = new HashSet<>();
}
public Pet(String nickname, int age, int trickLevel, String[] habits) {
    this.species = species;
    this.nickname = nickname;
    this.age = age;
    this.trickLevel = trickLevel;
    this.habits = new HashSet<>();
}
public Pet(String nickname, int age, Species species) {
    this.nickname = nickname;
    this.age = age;
    this.species = species;
    this.habits = new HashSet<>();
}

public abstract void respond();

// eat
private String eat;
public void setEat(String eat) {
    this.eat = eat;
}
public String getEat () {
    return this.eat;
}
public void eat() {
        System.out.println("Я їм!");
    }

 // respond
private String respond;
public void setRespond(String respond) {
    this.respond = respond;
}
public String getRespond () {
    return this.respond;
}

// describe
public abstract void describe();
// foul
private String foul;
public void setFoul(String foul) {
    this.foul = foul;
}
public String getFoul () {
    return this.foul;
}
public abstract void foul();

// species
public Species getSpecies() {
    return species;
}

public void setSpecies(Species species) {
    this.species = species;
}

// nickname
public String getNickname() {
    return nickname;
}

public void setNickname(String nickname) {
    this.nickname = nickname;
}

// age
public int getAge() {
    return age;
}

public void setAge(int age) {
    this.age = age;
}

// trickLevel
public int getTrickLevel() {
    return trickLevel;
}

public void setTrickLevel(int trickLevel) {
    this.trickLevel = trickLevel;
}

// habits
public String getHabits() {
    return habits.toString();
}

public void setHabits(String habits) {
    this.habits = Collections.singleton(habits);
}

    //

    @Override
    public String toString() {
        return species + "{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(new Set[]{habits}) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname);
    }
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Pet " + nickname + " is being finalized.");
        super.finalize();
    }



}