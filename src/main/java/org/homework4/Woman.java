package org.homework4;

import java.io.Serializable;
import java.util.Date;

public final class Woman extends Human  implements Serializable {

    public Woman(String name, String surname, Date birthDay) {
        super(name, surname , birthDay, Gender.FAMALE);
    }

    @Override
    public void uniqueAction() {
        System.out.println("Doing makeup...");
    }
    @Override
    public void greetPet (){
        System.out.println("Hello, my sweet " + getFamily().getPet().getNickname());
    }


}

