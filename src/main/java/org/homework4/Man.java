package org.homework4;

import java.io.Serializable;
import java.util.Date;

public final class Man extends Human  implements Serializable {

    public Man(String name, String surname, Date birthDate) {
        super(name, surname , birthDate, Gender.MALE);
    }

    @Override
    public void uniqueAction() {
        System.out.println("Repairing the car...");
    }

    @Override
    public void greetPet() {
        System.out.println("Hey, buddy! How are you doing, " + getFamily().getPet().getNickname() + "?");
    }

}
