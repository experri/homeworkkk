package org.homework4;

import java.io.Serializable;

public class Dog extends Pet  implements Serializable {
    public Dog(String nickname, int age) {
        super(nickname, age, Species.DOG);
    }

    @Override
    public void respond() {
        System.out.println("...");
    }

    @Override
    public void describe() {
        System.out.println("I am a Dog");
    }

    @Override
    public void foul() {
        System.out.println("Потрібно добре замести сліди...");
    }
}
