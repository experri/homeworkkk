package org.test;

import org.homework4.Family;
import org.homework4.Human;
import org.junit.Assert;
import org.junit.Test;
import org.homework4.Main;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class FamilyTest {

    @Test
    public void testToString() {
        Human mother2 = new Human("Mary", "Winston", 1989, 167);
        Human father2 = new Human("Piter", "Winston", 1987, 184);
        Family family2 = new Family(mother2, father2);
        String expected = "family:  \n" +
                "        \n" +
                "mother:{name='Mary', surname='Winston', birthDate=01/01/1970, iq=167, gender=null}task=\n" +
                "Name: Mary\n" +
                "Schedule:\n" +
                "\n" +
                "father:{name='Piter', surname='Winston', birthDate=01/01/1970, iq=184, gender=null}task=\n" +
                "Name: Piter\n" +
                "Schedule:\n" +
                "\n" +
                "children: [[]]\n" +
                "pet: []}";
        Assert.assertEquals(expected, family2.toString());
    }



//    @Test
//    public void testToString() {
//        Human mother = new Human("Vi", "Lancer", 2000);
//        String expected = "Human{name='Vi', surname='Lancer', year=2000, iq=0, pet=null{nickname='null', age=0, trickLevel=0, habits=nullnull{nickname='null'}}, mother=null, father=null}task=\n" +
//                "Name: Vi\n" +
//                "Schedule:" +
//                "\n";
//        Assert.assertEquals(expected, mother);
//
//    }
    @Test
    public void testDeleteChildBy() {
        Human mother = new Human("Vi", "Lancer", 2000, 187);
        Human father = new Human("Li", "Lancer", 2000, 187);
        Family family = new Family(mother, father);

        Human child1 = new Human("Alice", "Lancer", 2005, 124, mother, father);
        Human child2 = new Human("Anna", "Lancer", 2009,164, mother, father);
        family.addChild(child2);
        family.addChild(child1);

        assertEquals(4, family.countFamily());

        family.deleteChild(child2);

        assertEquals(3, family.countFamily());

        List<Human> children = family.getChildren();

        assertFalse(children.contains(child2));


    }

    @Test
    public void testAddChild() {
        Human mother = new Human("Jane", "Doe", 1980, 189);
        Human father = new Human("John", "Doe", 1975, 187);
        Family family = new Family(mother, father);

        Human child = new Human("Alice", "Doe", 2005, 197, mother, father);
        family.addChild(child);

        assertTrue(family.getChildren().contains(child));
    }

    @Test
    public void testCountFamily() {
        Human mother = new Human("Jane", "Doe", 1980, 200);
        Human father = new Human("John", "Doe", 1975,180);
        Family family = new Family(mother, father);
        family.addChild(new Human("Alice", "Doe", 2005, 190));
        Assert.assertEquals(3, family.countFamily());
    }


}
