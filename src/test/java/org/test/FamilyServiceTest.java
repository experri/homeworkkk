package org.test;

import org.homework4.*;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;

class FamilyServiceTest {

    private FamilyDao familyDao;
    private FamilyService familyService;

    @BeforeEach
    void setUp() {
        familyDao = new FakeFamilyDao();
        familyService = new FamilyService(familyDao);
    }

    @Test
    void getAllFamiliesTest() {
        List<Family> result = familyService.getAllFamilies();
        assertNotNull(result);
    }

    private static class FakeFamilyDao implements FamilyDao {
        @Override
        public List<Family> getAllFamilies() {
            // Повертаємо тестові дані
            return List.of(new Family((new Human("Da", "asd", 1/2/2024, 213)), new Human("Le", "asd", 3/4/2024, 124)));
        }

        @Override
        public Family getFamilyByIndex(int index) {
            return null;
        }

        @Override
        public boolean deleteFamilyByIndex(int index) {
            return false;
        }

        @Override
        public boolean deleteFamily(Family family) {
            return false;
        }

        @Override
        public void saveFamily(Family family) {

        }

        @Override
        public void createNewFamily(Human father, Human mother) {

        }

        @Override
        public Family bornChild(Family family, String boyName, String girlName) {
            return null;
        }

        @Override
        public Family adoptChild(Family family, Human child) {
            return null;
        }

        @Override
        public void deleteAllChildrenOlderThen(int age) {

        }

        @Override
        public int count() {
            return 0;
        }

        @Override
        public List<Pet> getPets(int familyIndex) {
            return null;
        }

        @Override
        public Family addPet(int familyIndex, Pet pet) {
            return null;
        }

        // Додайте інші методи FamilyDao за необхідності
    }

}
